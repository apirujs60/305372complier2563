from tool import *

class Language(object):
    Opcode=""
    Assembly=""
    Format=""
    def __init__(self,L):
        self.__class__=L
    def getList(self):
        return [self.Opcode,self.Assembly,self.Format]

class _p(Language):
    Opcode="0"
    Assembly="p"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        mem.write(SP-1,mem.read(value))
        return SP-1,CT+1,CY,BP,temp,temp1,temp2,PC+1
class _pc(Language):
    Opcode="1"
    Assembly="pc"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        if type(value)==type(0):
            value=convertIntTo16(value,3)
        mem.write(SP-1,value)
        return SP-1,CT+1,CY,BP,temp,temp1,temp2,PC+1
class _pr(Language):
    Opcode="2"
    Assembly="pr"
    Format="s"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        mem.write(SP-1,mem.read(BP+value))
        return SP-1,CT+1,CY,BP,temp,temp1,temp2,PC
class _cora(Language):
    Opcode="3"
    Assembly="cora"
    Format="s"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        mem.write(SP-1,convertIntTo16(BP+value,3))
        return SP-1,CT+1,CY,BP,temp,temp1,temp2,PC+1
class _asp(Language):
    Opcode="4"
    Assembly="asp"
    Format="s"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        SP+=value
        return SP,CT,CY,BP,temp,temp1,temp2,PC+1
class _call(Language):
    Opcode="5"
    Assembly="call"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        mem.write(SP-1,convertIntTo16(PC))
        PC=convert16ToInt(value)-1
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _ja(Language):
    Opcode="6"
    Assembly="ja"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        PC=convert16ToInt(value)-1
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _jct(Language):
    Opcode="7"
    Assembly="jct"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        if CT-=1 :
            PC = value-1;
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _jp(Language):
    Opcode="8"
    Assembly="jp"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        if mem.read(SP+1)>0:
            PC=value-1
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _jn(Language):
    Opcode="9"
    Assembly="jn"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        if mem.read(SP+1)<0:
            PC=value-1
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _jz(Language):
    Opcode="A"
    Assembly="jz"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        if mem.read(SP+1)==0:
            PC=value-1
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _jnz(Language):
    Opcode="B"
    Assembly="jnz"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        if mem.read(SP+1)!=0:
            PC=value-1
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _jodd(Language):
    Opcode="C"
    Assembly="jodd"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        if convert16ToInt(mem.read(SP+1))%2==1:
            PC=value-1
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _zon(Language):
    Opcode="D"
    Assembly="zon"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        if mem.read(SP+1)<=0:
            PC=value-1
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _jzop(Language):
    Opcode="E"
    Assembly="jzop"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        if mem.read(SP+1)>=0:
            PC=value-1
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _ret(Language):
    Opcode="F0"
    Assembly="ret"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        PC=convert16ToInt(mem.read(SP+1))-1
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _add(Language):
    Opcode="F1"
    Assembly="add"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        temp=mem.raed(SP+1)
        value,CY=add16bit(mem.raed(SP),temp)
        mem.write(SP,value)
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _sub(Language):
    Opcode="F2"
    Assembly="sub"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        temp=mem.read(SP++)
        mem.write(SP,sub16bit(mem.read(SP),temp))
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _stav(Language):
    Opcode="F3"
    Assembly="stav"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        temp=mem.read(SP+1)
        mem.write(convert16ToInt(mem.read(SP+1)),temp)
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _stva(Language):
    Opcode="F4"
    Assembly="stva"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        temp=mem.read(SP+1)
        mem.write(conver16ToInt(temp),mem.read(SP+1))
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _load(Language):
    Opcode="F5"
    Assembly="load"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        mem.write(SP,mem.read(mem.read(SP)))
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _awc(Language):
    Opcode="F6"
    Assembly="awc"
    Format="w"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        re,CY=add16bit(mem.read(SP)+value)
        mem.write(SP,re)
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _pwc(Language):
    Opcode="F7"
    Assembly="pwc"
    Format="w"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        mem.write(SP-1,value)
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _dupe(Language):
    Opcode="F8"
    Assembly="dupe"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        temp=mem.read(SP)
        mem.write(SP-1,temp)
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _esba(Language):
    Opcode="F9"
    Assembly="esba"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        mem.write(SP-1,BP)
        BP=convertIntTo16(SP,3)
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _reba(Language):
    Opcode="FA"
    Assembly="reba"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        SP=convert16ToInt(BP)
        BP=mem.read(SP++)
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _zsp(Language):
    Opcode="FB"
    Assembly="zsp"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        SP=0;
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _cmps(Language):  #???????????????????????????
    Opcode="FC"
    Assembly="cmps"
    Format="y"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        temp1=mem.read(SP+1)
        temp2=mem.read(SP)
        if temp2<temp1:
            value=(convert16ToInt(value)>>2)&1
        elif temp2==temp1:
            value=(convert16ToInt(value)>>1)&1
        elif temp2>temp1:
            value=(convert16ToInt(value))&1
        mem.write(SP,convertIntTo16(value))
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _cmpu(Language):  #???????????????????????????
    Opcode="FD"
    Assembly="cmpu"
    Format="y"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        temp1=mem.read(SP+1)
        temp2=mem.read(SP)
        if temp2<temp1:
            value=(convert16ToInt(value)>>2)&1
        elif temp2==temp1:
            value=(convert16ToInt(value)>>1)&1
        elif temp2>temp1:
            value=(convert16ToInt(value))&1
        mem.write(SP,convertIntTo16(value))
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _rev(Language):
    Opcode="FE"
    Assembly="rev"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        temp1=mem.read(SP+1)
        temp2=mem.read(SP)
        mem.write(SP-1,temp1)
        mem.write(SP,temp2)
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _shll(Language):
    Opcode="FF0"
    Assembly="shll"
    Format="z"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        mem.write(SP,convertIntTo16(conver16ToInt(mem.read(SP))<<value))
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _shrl(Language):
    Opcode="FF1"
    Assembly="shrl"
    Format="z"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        mem.write(SP,convertIntTo16(conver16ToInt(mem.read(SP))>>value))
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _shra(Language):
    Opcode="FF2"
    Assembly="shra"
    Format="z"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        mem.write(SP,convertIntTo16(conver16ToInt(mem.read(SP))>>value))
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _neg(Language):
    Opcode="FF3"
    Assembly="neg"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        #mem.write(SP,converIntTo16(neg16(mem.read(SP))))
        pass
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _mult(Language):
    Opcode="FF4"
    Assembly="mult"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        temp=mem.read(SP+1)
        mem.write(SP,convertIntTo16(convert16ToInt(mem.read(SP))*conver16ToInt(temp)))
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _div(Language):
    Opcode="FF5"
    Assembly="div"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        temp1=mem.read(SP+1)
        temp2=SP
        if temp1==0:
            CT=-1
        else :
            mem.write(SP,convertIntTo16(convert16ToInt(mem.read(SP))/conver16ToInt(temp)))
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _rem(Language):
    Opcode="FF6"
    Assembly="rem"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        temp=mem.read(SP+1)
        if temp==0:
            CT=-1
        else:
            mem.write(SP,convertIntTo16(convert16ToInt(mem.read(SP))%conver16ToInt(temp)))
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _addy(Language):
    Opcode="FF7"
    Assembly="addy"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        temp=mem.read(SP+1)
        mem.write(SP,add16bit(add16bit(mem.read(SP),temp),CY))
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _or(Language):
    Opcode="FF8"
    Assembly="or"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        mem.write(SP,converIntTo16(int(conver16ToInt(mem.read(SP+1)) | conver16ToInt(mem.read(SP))),4))
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _xo(Language):
    Opcode="FF9"
    Assembly="xo"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        mem.write(SP,converIntTo16(int(conver16ToInt(mem.read(SP+1)) ^ conver16ToInt(mem.read(SP))),4))
        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _and(Language):
    Opcode="FFA"
    Assembly="and"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):
        mem.write(SP,converIntTo16(int(conver16ToInt(mem.read(SP+1)) & conver16ToInt(mem.read(SP))),4))
        return SP,CT,CY,BP,temp,temp1,temp2,PC
    55555
class _flip(Language):
    Opcode="FFB"
    Assembly="flip"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _cali(Language):
    Opcode="FFC"
    Assembly="cali"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _sct(Language):
    Opcode="FFD"
    Assembly="sct"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _rot(Language):
    Opcode="FFE"
    Assembly="rot"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _psp(Language):
    Opcode="FFF0"
    Assembly="psp"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _bpbp(Language):
    Opcode="FFF1"
    Assembly="bpbp"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _pobp(Language):
    Opcode="FFF2"
    Assembly="pobp"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _pbp(Language):
    Opcode="FFF3"
    Assembly="pbp"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _bcpy(Language):
    Opcode="FFF4"
    Assembly="bcpy"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _uout(Language):
    Opcode="FFF5"
    Assembly="uout"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _sin(Language):
    Opcode="FFF6"
    Assembly="sin"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _sout(Language):
    Opcode="FFF7"
    Assembly="sout"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _hin(Language):
    Opcode="FFF8"
    Assembly="hin"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _hout(Language):
    Opcode="FFF9"
    Assembly="hout"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _ain(Language):
    Opcode="FFFA"
    Assembly="ain"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _aout(Language):
    Opcode="FFFB"
    Assembly="aout"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _din(Language):
    Opcode="FFFC"
    Assembly="din"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _dout(Language):
    Opcode="FFFD"
    Assembly="dout"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _noop(Language):
    Opcode="FFFE"
    Assembly="noop"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC
class _halt(Language):
    Opcode="FFFF"
    Assembly="halt"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2,PC):

        return SP,CT,CY,BP,temp,temp1,temp2,PC

language=[]
language.append(Language(_p))
language.append(Language(_pc))
language.append(Language(_pr))
language.append(Language(_cora))
language.append(Language(_asp))
language.append(Language(_call))
language.append(Language(_ja))
language.append(Language(_jct))
language.append(Language(_jp))
language.append(Language(_jn))
language.append(Language(_jz))
language.append(Language(_jnz))
language.append(Language(_jodd))
language.append(Language(_zon))
language.append(Language(_jzop))
language.append(Language(_ret))
language.append(Language(_add))
language.append(Language(_sub))
language.append(Language(_stav))
language.append(Language(_stva))
language.append(Language(_load))
language.append(Language(_awc))
language.append(Language(_pwc))
language.append(Language(_dupe))
language.append(Language(_esba))
language.append(Language(_reba))
language.append(Language(_zsp))
language.append(Language(_cmps))
language.append(Language(_cmpu))
language.append(Language(_rev))
language.append(Language(_shll))
language.append(Language(_shrl))
language.append(Language(_shra))
language.append(Language(_neg))
language.append(Language(_mult))
language.append(Language(_div))
language.append(Language(_rem))
language.append(Language(_addy))
language.append(Language(_or))
language.append(Language(_xo))
language.append(Language(_and))
language.append(Language(_flip))
language.append(Language(_cali))
language.append(Language(_sct))
language.append(Language(_rot))
language.append(Language(_psp))
language.append(Language(_bpbp))
language.append(Language(_pobp))
language.append(Language(_pbp))
language.append(Language(_bcpy))
language.append(Language(_uout))
language.append(Language(_sin))
language.append(Language(_sout))
language.append(Language(_hin))
language.append(Language(_hout))
language.append(Language(_ain))
language.append(Language(_aout))
language.append(Language(_din))
language.append(Language(_dout))
language.append(Language(_noop))
language.append(Language(_halt))
