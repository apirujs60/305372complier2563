def convert16ToInt(b16):
    index=len(b16)-1
    integer=0
    p=0
    while True:
        if index<=-1:
            return integer
        try:
            temp=int(b16[index])
        except ValueError:
            if b16[index]=="A" or b16[index]=="a":
                temp=10
            elif b16[index]=="B" or b16[index]=="b":
                temp=11
            elif b16[index]=="C" or b16[index]=="c":
                temp=12
            elif b16[index]=="D" or b16[index]=="d":
                temp=13
            elif b16[index]=="E" or b16[index]=="e":
                temp=14
            elif b16[index]=="F" or b16[index]=="f": 
                temp=15
            else:
                return "Error"
        integer+=power(16,p)*temp
        p+=1
        index-=1

def convertIntTo16(integer,bit):
    index=0
    b16=""
    while True:
        if index==bit:
            return b16
        temp=integer%16
        integer=(integer-temp)/16
        if temp==10:
            temp="A"
        elif temp==11:
            temp="B"
        elif temp==12:
            temp="C"
        elif temp==13:
            temp="D"
        elif temp==14:
            temp="E"
        elif temp==15:
            temp="F"
        else:
            temp=str(int(temp))
        b16=temp+b16
        index+=1
        

def power(number,p):
    if p==0:
        return 1
    else:
        return number*power(number,p-1)

def add16bit(va1,va2):
    temp=convert16ToInt(va1)+convert16ToInt(va2)
    temp=convertIntTo16(temp,5)
    return temp[1:],temp[:1]
def sub16bit(va1,va2):
    temp=convert16ToInt(va1)-convert16ToInt(va2)
    temp=convertIntTo16(temp,4)
    return temp
def right_shift(value,i):
    bit=[0,1,2,3]
    temp=value[3]
    for r in range(0,i):
        value=value[3:]+value[0:3]
    return value
def left_shift(value,i):
    bit=[0,1,2,3]
    temp=value[3]
    for r in range(0,i):
        value=value[1:]+value[:1]
    return value

def changeChar(STR,i,value):
    STRL=[x for x in STR]
    STRL[i]=value
    return "".join(STRL)

