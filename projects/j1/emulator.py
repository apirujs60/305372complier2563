from disassembler import *  #language
from Memory import *        #Memory
from delete_file import *   #reset_memory
path="memory/"      #folder of memories

def main():
    memoryRange=100
    mySM=StackMachine(path,memoryRange)
    mySM.start()

class StackMachine:
    def __init__(self,path,memoryRange):
        self.memory=Memory(path,memoryRange)
        self.SP=memoryRange
        self.PC=0
        self.CT=0
        self.CY=0
        self.BP=0
        self.temp=0
        self.temp1=0
        self.temp2=0
        
    def start(self):
        while True:
            user=self.inputCommand("?","")
            if user =="quit":
                break
            if user =="dump":
                self.dump()
            if user == "load":
                self.load()
            if user == "step":
                self.step()
            if user == "run":
                self.run()
            if user == "reset":
                self.SP=self.memory.memRange
                self.PC=0
                self.CT=0
                self.CY=0
                self.BP=0
                self.temp=0
                self.temp1=0
                self.temp2=0
                reset_memory(self.memory.path)

    def inputCommand(self,userProcess,addition):        ##'addition' is a string to print before get input.
                                                        ##'userProcess' is string to present user to know what progress is being. 
        user=input(userProcess+">"+addition)
        return str(user)

    def dump(self):
    ##    with open("memory.txt" , "r") as source:
    ##        for count in range(0,100):
    ##            readData = source.readline()
        mem=0
        temp="Program counter (PC): "+str(self.PC)+"\nstack pointer (SP): "+str(self.SP)+"\nCount register (CT): "+str(self.CT)+"\nCarry register (CY): "+str(self.CY)+"\nBase pointer (BP): "+str(self.BP)+"\nwork register 0(temp) : "+str(self.temp)+"\nwork register 1(temp1): "+str(self.temp1)+"\nwork register 2(temp2): "+str(self.temp2)
        print(temp)
        print("\t0\t1\t2\t3\t4\t5\t6\t7\t8\t9")
        for i in range(0,10):
            temp=str(i*10)
            for j in range(0,10):
                temp+="\t"+self.memory.read(mem)
                mem+=1
            print(temp)

    def load(self):
        fileName=self.inputCommand("load","Enter file name (with .elmct): ")
        try:       
            with open(fileName , "r") as source:
                readData = source.read()
        except IOError:
            print("No such file or directory: \""+fileName+"\"")

        lexicalList=lexicalAnalyzer(readData)
        opcodeList=syntaxAnalyzer(lexicalList)
        semanticAnalyzer(opcodeList)
                
        #errorHandler
        if errorCheck():
            return
        else:
            self.addCommand(lexicalList)

    def addCommand(self,command):
        index=0
        for i in command:
            self.memory.write(index,i)
            index+=1
            
    def step(self):
        IR=self.memory.read(self.PC) 
        command=syntaxAnalyzer([IR])
        if len(command)==0:
            print("instruction read error.")
        
        for i in language:
            if command.language.Opcode==i.Opcode:
                self.SP,self.CT,self.CY,self.BP,self.temp,self.temp1,self.temp2,self.PC = i.action(self.memory,command.Value,self.SP,self.CT,self.CY,self.BP,self.temp,self.temp1,self.temp2,self.PC)
                break
        self.PC+=1
        return True

    def run(self):
        if self.step():
            self.run()
    
if __name__== "__main__":
  main()
