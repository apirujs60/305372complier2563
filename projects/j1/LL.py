class _p(Language):
    Opcode="0"
    Assembly="p"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _pc(Language):
    Opcode="1"
    Assembly="pc"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _pr(Language):
    Opcode="2"
    Assembly="pr"
    Format="s"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _cora(Language):
    Opcode="3"
    Assembly="cora"
    Format="s"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _asp(Language):
    Opcode="4"
    Assembly="asp"
    Format="s"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _call(Language):
    Opcode="5"
    Assembly="call"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _ja(Language):
    Opcode="6"
    Assembly="ja"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _jct(Language):
    Opcode="7"
    Assembly="jct"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _jp(Language):
    Opcode="8"
    Assembly="jp"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _jn(Language):
    Opcode="9"
    Assembly="jn"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _jz(Language):
    Opcode="A"
    Assembly="jz"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _jnz(Language):
    Opcode="B"
    Assembly="jnz"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _jodd(Language):
    Opcode="C"
    Assembly="jodd"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _zon(Language):
    Opcode="D"
    Assembly="zon"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _jzop(Language):
    Opcode="E"
    Assembly="jzop"
    Format="x"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _ret(Language):
    Opcode="F0"
    Assembly="ret"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _add(Language):
    Opcode="F1"
    Assembly="add"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _sub(Language):
    Opcode="F2"
    Assembly="sub"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _stav(Language):
    Opcode="F3"
    Assembly="stav"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _stva(Language):
    Opcode="F4"
    Assembly="stva"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _load(Language):
    Opcode="F5"
    Assembly="load"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _awc(Language):
    Opcode="F6"
    Assembly="awc"
    Format="w"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _pwc(Language):
    Opcode="F7"
    Assembly="pwc"
    Format="w"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _dupe(Language):
    Opcode="F8"
    Assembly="dupe"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _esba(Language):
    Opcode="F9"
    Assembly="esba"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _reba(Language):
    Opcode="FA"
    Assembly="reba"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _zsp(Language):
    Opcode="FB"
    Assembly="zsp"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _cmps(Language):
    Opcode="FC"
    Assembly="cmps"
    Format="y"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _cmpu(Language):
    Opcode="FD"
    Assembly="cmpu"
    Format="y"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _rev(Language):
    Opcode="FE"
    Assembly="rev"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _shll(Language):
    Opcode="FF0"
    Assembly="shll"
    Format="z"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _shrl(Language):
    Opcode="FF1"
    Assembly="shrl"
    Format="z"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _shra(Language):
    Opcode="FF2"
    Assembly="shra"
    Format="z"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _neg(Language):
    Opcode="FF3"
    Assembly="neg"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _mult(Language):
    Opcode="FF4"
    Assembly="mult"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _div(Language):
    Opcode="FF5"
    Assembly="div"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _rem(Language):
    Opcode="FF6"
    Assembly="rem"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _addy(Language):
    Opcode="FF7"
    Assembly="addy"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _or(Language):
    Opcode="FF8"
    Assembly="or"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _xo(Language):
    Opcode="FF9"
    Assembly="xo"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _and(Language):
    Opcode="FFA"
    Assembly="and"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _flip(Language):
    Opcode="FFB"
    Assembly="flip"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _cali(Language):
    Opcode="FFC"
    Assembly="cali"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _sct(Language):
    Opcode="FFD"
    Assembly="sct"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _rot(Language):
    Opcode="FFE"
    Assembly="rot"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _psp(Language):
    Opcode="FFF0"
    Assembly="psp"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _bpbp(Language):
    Opcode="FFF1"
    Assembly="bpbp"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _pobp(Language):
    Opcode="FFF2"
    Assembly="pobp"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _pbp(Language):
    Opcode="FFF3"
    Assembly="pbp"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _bcpy(Language):
    Opcode="FFF4"
    Assembly="bcpy"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _uout(Language):
    Opcode="FFF5"
    Assembly="uout"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _sin(Language):
    Opcode="FFF6"
    Assembly="sin"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _sout(Language):
    Opcode="FFF7"
    Assembly="sout"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _hin(Language):
    Opcode="FFF8"
    Assembly="hin"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _hout(Language):
    Opcode="FFF9"
    Assembly="hout"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _ain(Language):
    Opcode="FFFA"
    Assembly="ain"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _aout(Language):
    Opcode="FFFB"
    Assembly="aout"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _din(Language):
    Opcode="FFFC"
    Assembly="din"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _dout(Language):
    Opcode="FFFD"
    Assembly="dout"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _noop(Language):
    Opcode="FFFE"
    Assembly="noop"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
class _halt(Language):
    Opcode="FFFF"
    Assembly="halt"
    Format="i"
    def action(self,mem,value,SP,CT,CY,BP,temp,temp1,temp2):

        return SP,CT,CY,BP,temp,temp1,temp2
    
language.append(Language(_p))
language.append(Language(_pc))
language.append(Language(_pr))
language.append(Language(_cora))
language.append(Language(_asp))
language.append(Language(_call))
language.append(Language(_ja))
language.append(Language(_jct))
language.append(Language(_jp))
language.append(Language(_jn))
language.append(Language(_jz))
language.append(Language(_jnz))
language.append(Language(_jodd))
language.append(Language(_zon))
language.append(Language(_jzop))
language.append(Language(_ret))
language.append(Language(_add))
language.append(Language(_sub))
language.append(Language(_stav))
language.append(Language(_stva))
language.append(Language(_load))
language.append(Language(_awc))
language.append(Language(_pwc))
language.append(Language(_dupe))
language.append(Language(_esba))
language.append(Language(_reba))
language.append(Language(_zsp))
language.append(Language(_cmps))
language.append(Language(_cmpu))
language.append(Language(_rev))
language.append(Language(_shll))
language.append(Language(_shrl))
language.append(Language(_shra))
language.append(Language(_neg))
language.append(Language(_mult))
language.append(Language(_div))
language.append(Language(_rem))
language.append(Language(_addy))
language.append(Language(_or))
language.append(Language(_xo))
language.append(Language(_and))
language.append(Language(_flip))
language.append(Language(_cali))
language.append(Language(_sct))
language.append(Language(_rot))
language.append(Language(_psp))
language.append(Language(_bpbp))
language.append(Language(_pobp))
language.append(Language(_pbp))
language.append(Language(_bcpy))
language.append(Language(_uout))
language.append(Language(_sin))
language.append(Language(_sout))
language.append(Language(_hin))
language.append(Language(_hout))
language.append(Language(_ain))
language.append(Language(_aout))
language.append(Language(_din))
language.append(Language(_dout))
language.append(Language(_noop))
language.append(Language(_halt))
