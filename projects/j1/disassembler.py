from Language import * #language
from tool import *
errorHandler=[]

_16bit=["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","a","b","c","d","e","f"]

def main(source_file):          
    with open(source_file , "r") as source:
        #process
        readData = source.read()

    lexicalList=lexicalAnalyzer(readData)
    opcodeList=syntaxAnalyzer(lexicalList)
    semanticAnalyzer(opcodeList)
        
    #errorHandler
    if errorCheck():
        return
    else:
        command=intermediateGenerate(opcodeList)
    if errorCheck():
        return
    else:
        generate(command)
    return command.strip()
        
def errorCheck():
    if len(errorHandler)!=0:
        for printE in errorHandler:
            print(printE)
        return True
    else:
        return False
        
def lexicalAnalyzer(readData):
    readData=readData.strip().split("\n")
    readData=[x.strip() for x in readData]
    readData=[x for x in readData if x!=""]
    return readData

def syntaxAnalyzer(lexicalList):
    error=""
    gen=[]
    
    for i in range(0,len(lexicalList)):
        error="|line"+str(i+1)+"> "
        if len(lexicalList[i])>8:
            error+="source is more then 2 word."
        langCheck=True
        s=0
        match=False
        L=0
        index=0
        while True:
            if L==len(language) or s==len(lexicalList[i]) :
                break
                
            if language[L].Opcode[index]==lexicalList[i][s]:
                if index==len(language[L].Opcode)-1:  #match opcode with assambly.
                    match=True
                    gen.append(Source(language[L],lexicalList[i][index+1:].strip()))
                    break
                else:
                    index+=1
                    s+=1
            else:
                L+=1
                s=0
                index=0

        if not match:
            error+="Invalid Opcode."

        if error!="|line"+str(i+1)+"> ":
            errorHandler.append(error)
    return gen

def semanticAnalyzer(opcodeList):
    error=""
    for i in range(0,len(opcodeList)):
        error="|line"+str(i+1)+"> "
        if opcodeList[i].Language.Format=="s":
            value=convert16ToInt(opcodeList[i].Value)
            if value>4095 or value < -4095:
                error+="Format value ("+opcodeList[i].Language.Format+") out of range."
        elif opcodeList[i].Language.Format=="x":
            value=convert16ToInt(opcodeList[i].Value)
            if value>4095:
                error+="Format value ("+opcodeList[i].Language.Format+") out of range."
        elif opcodeList[i].Language.Format=="y":
            value=convert16ToInt(opcodeList[i].Value)
            if value>255:
                error+="Format value ("+opcodeList[i].Language.Format+") out of range."
        elif opcodeList[i].Language.Format=="z":
            value=convert16ToInt(opcodeList[i].Value)
            if value>15:
                error+="Format value ("+opcodeList[i].Language.Format+") out of range."
        elif opcodeList[i].Language.Format=="w":
            value=convert16ToInt(opcodeList[i].Value)
            if value>65535 or value <-65535:
                error+="Format value ("+opcodeList[i].Language.Format+") out of range."
        elif opcodeList[i].Language.Format=="i":
            if opcodeList[i].Value.strip() != "":
                error+=" Format ("+opcodeList[i].Language.Format+") error."
        if error!="|line"+str(i+1)+"> ":
            errorHandler.append(error)
            
                
def intermediateGenerate(opcodeList):
    command=""
    error=""
    for i in range(0,len(opcodeList)):
        if opcodeList[i].Language.Format == "i":
            command+=opcodeList[i].Language.Assembly+"\n"
        else:
            command+=opcodeList[i].Language.Assembly+" "+str(convert16ToInt(opcodeList[i].Value))+"\n"    
    return command

def generate(command):
    file_path = 'output.asm'
    try:
        with open(file_path) as newFile:
            print("file's name is already exists.")
            return
    except IOError:
        # If not exists, create the file
        with open(file_path, 'w+') as newFile:
            newFile.write(command.strip())
            print("generated.")
    
class Source():
    def __init__(self,memList,value):
        self.Language=memList
        self.Value=value

if __name__== "__main__":
  main("source.elmct")
