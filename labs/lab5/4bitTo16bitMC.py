errorHandler=[]
language=[["0000","STOP"],["9001","READ"],["9002","PRINT"],["1","ADD"],["2","SUB"],["3","STO"],["4","STA"],["5","LOAD"],["6","B"],["7","BZ"],["8","BP"]]
def main():
    
    
    with open("source.txt" , "r") as source:
        #process
        readData = source.read()
        lexicalList=lexicalAnalyzer(readData)
        syntaxAnalyzer(lexicalList)
        semanticAnalyzer(lexicalList)
        
        #errorHandler
        if errorCheck():
            return
        else:
            command=intermediateGenerate(lexicalList)
        if errorCheck():
            return
        else:
            generate(command)
        print(lexicalList)
        print(command)
        
def errorCheck():
    if len(errorHandler)!=0:
        for printE in errorHandler:
            print(printE+"\n")
        return True
    else:
        return False

def lexicalAnalyzer(readData):
    readData=readData.strip().split("\n")
    readData=[x.strip() for x in readData]
    readData=[x for x in readData if x!=""]
    return readData

def syntaxAnalyzer(lexicalList):
    error=""
    for i in range(0,len(lexicalList)):
        error="line"+str(i+1)+":"
        if len(lexicalList[i])!=4:
            error+="Error - code is not 4-digit."
        try:
            int(lexicalList[i])
        except ValueError:
            error+="Error - code is not number digit."
        if error!="line"+str(i+1)+":":
            errorHandler.append(error)
                
def semanticAnalyzer(lexicalList):
    error=""
    for i in range(0,len(lexicalList)):
        error="line"+str(i+1)+":"
        command=int(lexicalList[i][0])
        if command<=8 and command>=1:
            mem= int(lexicalList[i][1:])
            if mem<0 or mem>100:
                error+="Error - memory out of range."
        
        if error!="line"+str(i+1)+":":
            errorHandler.append(error)
            
                
def intermediateGenerate(lexicalList):
    command=""
    error=""
    
    for i in range(0,len(lexicalList)):
        binary16=[]
        for j in [3,2,1,0]:
            temp=int(lexicalList[i][j])
            binary4=""
            for k in range(0,4):
                binary4+=str(temp%2)
                temp=int(temp/2)                
            binary16.append(binary4[3]+binary4[2]+binary4[1]+binary4[0])
        command+=str(binary16[3])+str(binary16[2])+str(binary16[1])+str(binary16[0])#+" "
    return command

def generate(command):
    file_path = 'elmc.txt'
    try:
        with open(file_path) as newFile:
            print("file's name is already exists.")
            return
    except IOError:
        # If not exists, create the file
        with open(file_path, 'w+') as newFile:
            newFile.write(command.strip())
            print("generated.")

if __name__== "__main__":
  main()
