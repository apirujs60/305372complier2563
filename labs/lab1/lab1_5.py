def main():
    with open("source.src" , "r") as source:
        data = source.read()
        print("data: \""+data+"\"")
        createCS(data)

def createCS(data):
    file_path = 'newCS.cs'
    try:
        with open(file_path) as newFile:
            print("C# : file's name is already exists.")
    except IOError:
        # If not exists, create the file
        with open(file_path, 'w+') as newFile:
            command="""
using System;

namespace ReadLine
{
    class Program
    {
        static void Main(string[] args)
        {
            """+"Console.WriteLine(\""+data+"\");"+"""
        }
    }
}
"""
            newFile.write(command)

    
if __name__== "__main__":
  main()
