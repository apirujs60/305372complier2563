def main():
    with open("source.src" , "r") as source:
        data = source.read()
        print("data: \""+data+"\"")
        createTXT(data)


def createTXT(data):
    file_path = 'newText.txt'
    try:
        with open(file_path) as newFile:
            print("TXT : file's name is already exists.")
    except IOError:
        # If not exists, create the file
        with open(file_path, 'w+') as newFile:
            newFile.write(data)

  
if __name__== "__main__":
  main()
