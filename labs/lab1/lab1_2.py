def main():
    with open("source.src" , "r") as source:
        data = source.read()
        print("data: \""+data+"\"")
        createPY(data)

def createPY(data):
    file_path = 'newPython.py'
    try:
        with open(file_path) as newFile:
            print("PY : file's name is already exists.")
    except IOError:
        # If not exists, create the file
        with open(file_path, 'w+') as newFile:
            command="print(\""+data+"\")"
            newFile.write(command)

 
if __name__== "__main__":
  main()
