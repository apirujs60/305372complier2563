def main():
    with open("source.src" , "r") as source:
        data = source.read()
        print("data: \""+data+"\"")
        #createTXT(data)
        #createPY(data)
        #createC(data)
        #createCPP(data)
        #createCS(data)

def createTXT(data):
    file_path = 'newText.txt'
    try:
        with open(file_path) as newFile:
            print("TXT : file's name is already exists.")
    except IOError:
        # If not exists, create the file
        with open(file_path, 'w+') as newFile:
            newFile.write(data)

def createPY(data):
    file_path = 'newPython.py'
    try:
        with open(file_path) as newFile:
            print("PY : file's name is already exists.")
    except IOError:
        # If not exists, create the file
        with open(file_path, 'w+') as newFile:
            command="print(\""+data+"\")"
            newFile.write(command)

def createC(data):
    file_path = 'newC.c'
    try:
        with open(file_path) as newFile:
            print("C : file's name is already exists.")
    except IOError:
        # If not exists, create the file
        with open(file_path, 'w+') as newFile:
            command="""
#include<stdio.h>
int main() 
{ 
   """+"printf(\""+data+"\");"+""" 
   return 0; 
} 
"""
            newFile.write(command)

def createCPP(data):
    file_path = 'newCPP.cpp'
    try:
        with open(file_path) as newFile:
            print("CPP : file's name is already exists.")
    except IOError:
        # If not exists, create the file
        with open(file_path, 'w+') as newFile:
            command="""
#include<stdio.h>
int main() 
{ 
   """+"printf(\""+data+"\");"+""" 
   return 0; 
} 
"""
            newFile.write(command)

def createCS(data):
    file_path = 'newCS.cs'
    try:
        with open(file_path) as newFile:
            print("C# : file's name is already exists.")
    except IOError:
        # If not exists, create the file
        with open(file_path, 'w+') as newFile:
            command="""
using System;

namespace ReadLine
{
    class Program
    {
        static void Main(string[] args)
        {
            """+"Console.WriteLine(\""+data+"\");"+"""
        }
    }
}
"""
            newFile.write(command)

    
if __name__== "__main__":
  main()
