def main():
    with open("source.src" , "r") as source:
        data = source.read()
        print("data: \""+data+"\"")
        createC(data)


def createC(data):
    file_path = 'newC.c'
    try:
        with open(file_path) as newFile:
            print("C : file's name is already exists.")
    except IOError:
        # If not exists, create the file
        with open(file_path, 'w+') as newFile:
            command="""
#include<stdio.h>
int main() 
{ 
   """+"printf(\""+data+"\");"+""" 
   return 0; 
} 
"""
            newFile.write(command)

    
if __name__== "__main__":
  main()
