def main():
    
    with open("source.src" , "r") as source:
        data = source.read()
        #print("data: \""+data+"\"")
        try:
            intData=int(data)
            if((intData>=0)and(intData<=2147483647)):
                createCPP(data)
                print("CPP generated.")
            else:
                print("ERROR int: data is out of range.")
        except ValueError:
            print("ERROR: data is not a number.")
        

def createCPP(data):
    file_path = 'newCPP.cpp'
    try:
        with open(file_path) as newFile:
            print("CPP : file's name is already exists.")
    except IOError:
        # If not exists, create the file
        with open(file_path, 'w+') as newFile:
            command="""
#include<stdio.h>
int main() 
{
   int number="""+data+""";
   printf("%d",number);
   return 0; 
} 
"""
            newFile.write(command)


if __name__== "__main__":
  main()
