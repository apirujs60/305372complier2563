from lab5 import *
from Memory import *
path="memory/"      #folder of memories

def main():
    myLMC=LMC(path,100)
    myLMC.start()

class LMC:
    def __init__(self,path,memoryRange):
        self.memory=Memory(path,memoryRange)
        self.AC=0
        self.PC=0
        self.IR=0
        print("*** Welcome to an Extended Little Man Computer Emulator! ***")
        
    def start(self):
        while True:
            user=self.inputCommand("?","")
            if user =="quit":
                break
            if user =="dump":
                self.dump()
            if user == "load":
                self.load()
            if user == "step":
                self.step()
            if user == "run":
                self.run()
            if user == "reset":
                self.AC=0
                self.PC=0
                self.IR=0

    def inputCommand(self,userProcess,addition):        ##'addition' is a string to print before get input.
                                                        ##'userProcess' is string to present user to know what progress is being. 
        user=input(userProcess+">"+addition)
        return str(user)

    def dump(self):
    ##    with open("memory.txt" , "r") as source:
    ##        for count in range(0,100):
    ##            readData = source.readline()
        mem=0
        temp="Accumulator (AC): "+str(self.AC)+"\nProgram Counter (PC): "+str(self.PC)+"\nInstruction Register (IR): "+str(self.IR)
        print(temp)
        print("\t0\t1\t2\t3\t4\t5\t6\t7\t8\t9")
        for i in range(0,10):
            temp=str(i*10)
            for j in range(0,10):
                temp+="\t"+self.memory.read(mem)
                mem+=1
            print(temp)

    def load(self):
        fileName=self.inputCommand("load","Enter file name (with .txt): ")
        try:       
            with open(fileName , "r") as source:
                #process
                readData = source.read()
                lexicalList=lexicalAnalyzer(readData)
                syntaxAnalyzer(lexicalList)
                semanticAnalyzer(lexicalList)
                
                #errorHandler
                if errorCheck():
                    return
                else:
                    self.addCommand(lexicalList)
        except IOError:
            print("No such file or directory: \""+fileName+"\"")

    def addCommand(self,command):
        index=0
        for i in command:
            self.memory.write(index,i)
            index+=1
            
    def step(self):
        self.IR=self.memory.read(self.PC)
        command=intermediateGenerate([self.IR])
        command=command.strip() #get rid of "\n"
        command=command.split("\t")
        #command[0] is command to do the thing.     command[1] is number-data to interact.
        if command[0]=="STOP":
            self.PC+=1
            return False
        elif command[0]=="ADD":
            self.AC=int(self.memory.read(int(command[1])))+self.AC
        elif command[0]=="SUB":
            self.AC=int(self.memory.read(int(command[1])))-self.AC
        elif command[0]=="STO":
            self.memory.write(int(command[1]),self.AC)
        elif command[0]=="STA":
            pass
        elif command[0]=="LOAD":
            self.AC=self.memory.read(command[1])
        elif command[0]=="B":
            self.PC=command[1]
        elif command[0]=="BZ":
            if self.ACC <= 0:
                self.PC=command[1]
        elif command[0]=="BP":
            if self.ACC >= 0:
                self.PC=command[1]
        elif command[0]=="READ":
            self.AC=int(self.inputCommand("READ",""))
        elif command[0]=="PRINT":
            print("Output:"+str(self.AC))

        self.PC+=1
        return True

    def run(self):
        if self.step():
            self.run()
    
if __name__== "__main__":
  main()
