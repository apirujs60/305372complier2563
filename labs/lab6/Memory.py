class Memory:
    def __init__(self, path, Range):
        self.path=path
        self.memRange=Range
        
    def read(self,index):
        if index<0 or index>self.memRange:
            return
        name=self.path+"m"+str(index)+".txt"
        data=""
        try:
            with open(name, "r") as source:
                data=source.read()
                return data.strip()
        except IOError:
            return "0000"
        
    def write(self,index,data):
        if index<0 or index>self.memRange:
            return
        name=self.path+"m"+str(index)+".txt"
        strData=""
        for i in range(0,4-len(str(data))):
            strData+="0"
        strData+=str(data)
        try:
            with open(name,'w') as source:
                source.write(strData)
        except IOError:
            with open(name, 'w+') as newFile:
                newFile.write(strData)
        
