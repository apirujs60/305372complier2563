errorHandler=[]
MachineCode=["0000","9001","9002","00","10","20","30","40","50","60","70","80"]
Assembly=["STOP","READ","PRINT","DAT","ADD","SUB","STO","STA","LOAD","B","BZ","BP"]
Terminal="abcdefghijklmnopqrstuvwxyz"
TerminalUpper=Terminal.upper()
Number="1234567890"
command_line=[]
def main():
    with open("code.txt" , "r") as source:
        #process
        readData = source.read()
        lexicalAnalyzer(readData)
        syntaxAnalyzer()
        semanticAnalyzer()
        
        #errorHandler
        if errorCheck():
            return
        else:
            command=intermediateGenerate()
        if errorCheck():
            return
        else:
            generate(command)
        for i in command:
            print(i)
        #print(command)
        
def errorCheck():
    if len(errorHandler)!=0:
        for printE in errorHandler:
            print(printE)
        return True
    else:
        return False

def lexicalAnalyzer(readData):
    readData=readData.strip().split("\n")
    readData=[x.strip() for x in readData]
    readData=[x for x in readData if x!=""]
    for line in readData:
        label="Null"
        command="Null"
        comment="Null"
        temp=line
        
        if temp.find(":")>=0:
            label=temp[:temp.find(":")].strip()
            temp=temp[temp.find(":")+1:]
        if temp.find(";")>=0:
            if temp[:temp.find(";")].strip()!="":
                command=temp[:temp.find(";")].strip()
            comment=temp[temp.find(";")+1:]
        elif temp!="":
            command=temp
        command_line.append(Command_line(label,command,comment))

def syntaxAnalyzer():
    error=""
    for i in range(0,len(command_line)):
        error="line"+str(i+1)+":"
        command_list=command_line[i].Command.split()
        command_list=[x for x in  command_list if x!="Null" or x!=""]
        #################Analyzing label     
        if command_line[i].Label == "":                         
            error+="SyntaxError: invalid syntax."           #error [!= ""] mean in command line had ":" in line but no such a label before it.
            errorHandler.append(error)
            continue
        elif command_line[i].Label != "" and command_line[i].Label != "Null":
            check=grammar_parsing(command_line[i].Label)
            if len(command_line[i].Label.split())>1:
                error+="SyntaxError: invalid syntax."
                errorHandler.append(error)
                continue
            if check=="":
                if len(command_list)==0:
                    error+="SyntaxError: invalid syntax."
                    errorHandler.append(error)
                    continue
            else:
                error+=check
                errorHandler.append(error)
                continue
            
        ###############Analyzing command
        if len(command_list)>0:
            if command_list[0] not in Assembly:     #this is not Assembly.
                if len(command_list)<2:
                    if command_list[0]=="Null":
                        continue    
                    error+="SyntaxError: invalid syntax."
                    errorHandler.append(error)
                    continue
                check=grammar_parsing(command_list[0])
                
                if check=="":                       #this is value.
                    
                    if command_list[1] not in Assembly:                     #second command much be only DAT in this Machine feature.
                        error+="SyntaxError: invalid syntax."               #if first command is not Assembly the second command much be Assembly(DAT).
                        errorHandler.append(error)
                        continue
                    else:                                                   #if first command is not Assembly and the second command is Assembly next mish be only number.
                        if len(command_list)<3 or len(command_list)>3:      #no more 3 command in one line: Assembly-Num , Assembly-value , Value-Assembly-Num.
                            error+="SyntaxError: invalid syntax."
                            errorHandler.append(error)
                            continue
                        try:                                #Value-assembly-?
                            int(command_list[2])
                        except ValueError:
                            error+="SyntaxError: invalid syntax."
                            errorHandler.append(error)
                            continue
                else:   #first command is not Assembly and not a value.
                    error+=check
                    errorHandler.append(error)
                    continue
            else:
                #first command is Assembly,second command mush be number or value.
                if len(command_list)>1:
                    
                    check=grammar_parsing(command_list[1])      #Assembly-?
                    if check != "":                             #second is not value. check is it a number?
                        try:                                    
                            int(command_list[1]) #pass
                        except ValueError:
                            error+=check
                            errorHandler.append(error)
                            continue

def grammar_parsing(label):     #if parsing have an issue return error.
    if label=="":
        return "SyntaxError: invalid syntax."
    state=0
    index=0
    LA=label[index:2]
    while True:
        
        if state==0:
            if (Terminal.find(LA[:1])>=0 or TerminalUpper.find(LA[:1])>=0) and LA[:1]!= "":
                #print(LA[:1]!= "" and (Terminal.find(LA[:1])>=0 or TerminalUpper.find(LA[:1])>=0) )
                if (Terminal.find(LA[1:2])>=0 or TerminalUpper.find(LA[1:2])>=0) and LA[1:2]!= "":
                    pass
                elif Number.find(LA[1:2])>=0 and LA[1:2]!= "":
                    state=1
                elif LA[1:2]=="" or LA[1:2]==" ":
                    return "" #final state
                else:
                    state=3
            else:
                state=3
        elif state==1:
            if Number.find(LA[:1])>=0 and LA[:1]!= "":
                if (Terminal.find(LA[1:2])>=0 or TerminalUpper.find(LA[1:2])>=0) and LA[1:2]!= "":
                    state=0
                elif Number.find(LA[1:2])>=0 and LA[1:2]!= "":
                    pass
                elif LA[1:2]=="" or LA[1:2]==" ":
                    
                    return "" #final state
                else:
                    state=3
            elif LA[:1]=="" or LA[:1]==" ":
                return "" #final state
            else:
                
                state=3
        elif state==3:
            return "SyntaxError: invalid syntax."
        index=index+1
        LA=label[index:index+2]
            
def semanticAnalyzer():
    error=""
    for i in range(0,len(command_line)):
        error="line"+str(i+1)+":"
        command=command_line[i].Command.split()
        if command[0]=="Null":
            continue
        if command[0]==Assembly[0] or command[0]==Assembly[1] or command[0]==Assembly[2]:    #"0000 9001 9002" 
            if len(command)>1:                                                                  #do not take address.
                error+="AssemblyError: "+command[0]+" does not takes address number but "+str(len(command)-1)+" were given."
        elif command[0] not in Assembly:
            if command[1] not in Assembly:                      #common-value take Assembly and one address number.
                error+="AssemblyError: invalid Assembly code "+command[1]+"."
            else:
                if int(command[2])>100 or int(command[2])<0:
                    error+="ValueError: Value out of range."
        else:                                                   #"1-8" Assembly
            if type(command[1])==type(" "):
                pass
            elif int(command[1])>100 or int(command[1])<0:
                error+="ValueError: Value out of range."
                
        if error!="line"+str(i+1)+":":
            errorHandler.append(error)
            
                
def intermediateGenerate():
    generate=[]
    all_label=[]
    error=""
    dont_done=[]
    
    for i in range(0,len(command_line)):
        error="line"+str(i+1)+":"
        command=command_line[i].Command.split()
        if command[0]=="Null":
            continue
        if command_line[i].Label!="Null":
            error+=label_check(command_line[i].Label,all_label)
            all_label.append([command_line[i].Label,str(len(generate))])
        if command[0] not in Assembly:
            index=1     #it is value,check next command.
            error+=label_check(command[0],all_label)
            all_label.append([command[0],str(len(generate))])
        else:
            index=0
          
        for j in range(0,len(Assembly)):
            if j<=2:                            #9001 9002 0000
                if command[index]==Assembly[j]:
                    generate.append(MachineCode[j])
                    break
            else:                               #0 1 2 3 4 5 6 7 8 command
                if command[index]==Assembly[j]:
                    try:
                        number=int(command[index+1])
                        if len(str(command[index+1]))==1:
                            number="0"+str(number)
                        else:
                            number=str(number)
                        generate.append(MachineCode[j]+number)
                        break
                    except ValueError:
                        dont_done.append([len(generate),command[index+1]])
                        generate.append(MachineCode[j])
                        break
        if error!="line"+str(i+1)+":":
            errorHandler.append(error)
            return generate
        #check+=1
    check=0
    for value in dont_done:
        for label in all_label:
            if label[0]==value[1]:
                if len(label[1])==1:
                    address="0"+label[1]
                else:
                    address=label[1]
                generate[value[0]]+=address
                check+=1
        if check==0:
            error="line"+str(value[0]+1)+":ValueError: can not find "+value[1]+"."
            errorHandler.append(error)
    return generate

def label_check(label,all_label):
    for i in all_label:
        if label in i:
            return "ValueError: There is "+label+" more then one."
    return ""
            

def generate(command):
    file_path = 'output.txt'
    print_=""
    for line in command:
        print_+=line+"\n"
    try:
        with open(file_path) as newFile:
            print("file's name is already exists.")
            return
    except IOError:
        # If not exists, create the file
        with open(file_path, 'w+') as newFile:
            newFile.write(print_)
            print("generated.")

class Command_line:
    def __init__(self,label,command,comment):
        self.Label=label
        self.Command=command
        self.Comment=comment

if __name__== "__main__":
  main()

