do_action=[]
state_transition=[]
source_list=[]
comment_list=[]
source_list0=[]
comment_list0=[]
state=0
index=0
source=""
charac=""
input_map=[]
read_data=""

def main():
    global do_action
    global state_transition
    global source_list
    global comment_list
    global source_list0
    global comment_list0
    global state
    global index
    global source
    global charac
    global input_map
    global read_data
    
#////////////////////////////////////////////////////init
    
    with open("code.txt" , "r") as file:
        read_data=file.read()
    with open("action.csv","r") as file:
        action=file.read()
    with open("transition.csv","r") as file:
        transition=file.read()
        
    action=action.split("\n")
    transition=transition.split("\n")

    if action[0].split(",")==transition[0].split(","):
        temp = action[0].split(",")
        del temp[0]
        for i in range(0,len(temp)):
            if temp[i]=="\\n":
                temp[i]='\n'
        input_map=temp
    else:
        print("action table column and transition table column do not mach.")
        return
    
    for i in range(1,len(action)):
        temp=action[i].split(",")
        del temp[0]
        do_action.append(temp)
        
    for i in range(1,len(transition)):
        temp=transition[i].split(",")
        del temp[0]
        for j in range(0,len(temp)):
            temp[j]=int(temp[j])
        state_transition.append(temp)
        
#////////////////////////////////////////////////////process
        
    while(True):
        num_input=get_input(read_data[index:index+1])
        exec(do_action[state][num_input])
        state=state_transition[state][num_input]
        if state==3:
            break
        else:
            index+=1

#///////////////////////////////////////////////////result
    source_list=[x for x in source_list0 if x != ""]
    comment_list=[x for x in comment_list0 if x != ""]
    
    print("source\n")
    for i in source_list:
        print(":"+i)
    print("\ncomment\n")
    for i in comment_list:
        print(":"+i)

def add_source():
    global index
    global read_data
    global source
    source+=read_data[index:index+1]
def add_charac():
    global index
    global read_data
    global charac
    charac+=read_data[index:index+1]
def source_append():
    global index
    global read_data
    global source
    global source_list0
    source+=read_data[index:index+1]
    source_list0.append(source.strip())
    source=""
    
def source_append2():
    global index
    global read_data
    global source
    global source_list0
    source+=read_data[index:index+1]
    source=source[:len(source)-2]
    source_list0.append(source.strip())
    source=""

def comment_append():
    global index
    global read_data
    global charac
    global comment_list0
    charac+=read_data[index:index+1]
    comment_list0.append(charac.strip())
    charac=""

def get_input(char):
    global input_map
    if char=="":    #* = else string. @ = end of string.
        char="@"
    mapR=-1
    if char=="\n":
        return 2
    for i in range(0,len(input_map)):
        if input_map[i] == char:
            mapR=i
    if mapR==-1:
        char="*"
        for i in range(0,len(input_map)):
            #print(char)
            if input_map[i] == char:
                mapR=i
    return mapR


if __name__== "__main__":
    main()
