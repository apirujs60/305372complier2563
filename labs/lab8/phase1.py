state0="0"  #source
state1="1"  #/
state2="2"  #// comment
test=["1","2","3"]
source_list=[]
comment_list=[]

def main():
    
    with open("code.txt" , "r") as file:
        read_data=file.read()
    state=state0
    index=0
    end=len(read_data)
    source=""
    charac=""
    source_list0=[]
    charac_list=[]
    
    while True:
        if state == state0:

            if index == end:
                source_list0.append(source.strip())
                source=""
                break

            source+=read_data[index]
            if read_data[index] == "/":
                state = state1
            elif read_data[index] == "\n":
                source_list0.append(source.strip())
                source=""
            else:
                pass
                
        elif state == state1:

            if index == end:
                source_list0.append(source.strip())
                source=""
                break

            source+=read_data[index]
            if read_data[index] == "/":
                source=source[:len(source)-2]
                source_list0.append(source.strip())
                source=""
                state = state2
            elif read_data[index]=="\n":
                source_list0.append(source.strip())
                source=""
                state = state0 
            else:
                state = state0
                    
        elif state == state2:

            if index == end:
                charac_list.append(charac.strip())
                charac=""
                break

            charac+=read_data[index]
            if read_data[index] == "\n":
                charac_list.append(charac.strip())
                charac=""
                state = state0
            else:
                pass
        index+=1

    source_list=[x for x in source_list0 if x != ""]
    comment_list=[x for x in charac_list if x != ""]
    
    print("source\n")
    for i in source_list:
        print(":"+i)
    print("\ncomment\n")
    for i in comment_list:
        print(":"+i)

if __name__== "__main__":
  main()

#12/10/2020 6.00-6.50 design
  #6.56-7.24  10.23-10.40 10.59-11.27 code
  #12.05-12.06 test
  #12.06-12.08 debug test
  #12.14
  #12.08-12.45 test debug done
