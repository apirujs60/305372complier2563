errorHandler=[]
def main():
    language=[["plus","+"],["minus","-"],["multiply","*"],["divide","/"],["none","\\"]]
    idenWord=[]
    
    with open("source.src" , "r") as source:
        data = source.read()
        #print("data: \""+data+"\"")
        data=data.strip().split()           ####################   Lexical Analyzer   ######################
        for word in data:                   #identify               ##############   Syntax Analyzer   ##############
            try:
                intData=int(word)                               #number word        
                idenWord.append(["number",word])
                continue
            except ValueError:                                  #not a number,it is string.
                for identify in language:
                    unique=False
                    if word==identify[1]:
                        idenWord.append([identify[0],word])     #unique words
                        unique=True                                        
                        break
                if unique==False:
                    idenWord.append(["identifier",word])        #unknow words
                                                                #valid word ,not implement.
        #print(idenWord)
        semanticAnalyzer(idenWord)
        if len(errorHandler)==0:
            createCPP(idenWord)
        else:
            for error in errorHandler:
                print(error+"\n")
            
            
def semanticAnalyzer(idenWord):                        ############## semanticAnalyzer ##############
    for i in idenWord:
        if i[0]=="number":
            if int(i[1])<0 or int(i[1])>2147483647:
                errorHandler.append("integer Error: Value out of range.")
                return


                
def createCPP(data):
    file_path = 'newCPP.cpp'
    printWord=[]
    try:
        with open(file_path) as newFile:
            print("CPP : file's name is already exists.")
            return
    except IOError:
        # If not exists, create the file
        for i in data:
            if i[0]=="number":
                printWord.append(str(i[1]))
        with open(file_path, 'w+') as newFile:
            command="""
#include<stdio.h>
int main() 
{
   char all[1000]=\"number:"""+",".join(printWord)+"""\";
   printf("%s",all);
   return 0; 
} 
"""
            newFile.write(command)
            print("CPP generated.")


if __name__== "__main__":
  main()
